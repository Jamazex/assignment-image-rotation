#ifndef UNTITLED1_IMAGE_ROT_STUB_H
#define UNTITLED1_IMAGE_ROT_STUB_H

#include <stdbool.h>
#include <stdint.h>

#include "utils.h"

void bmp_against_clockwise_rotate (const char* input_file, const char* output_file);

#endif
