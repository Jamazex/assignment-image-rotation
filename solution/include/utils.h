#ifndef UNTITLED1_UTILS_H
#define UNTITLED1_UTILS_H

#include <stdio.h>
#include <stdlib.h>

enum main_arguments_status{
    ENOUGH_ARGUMENTS,
    TOO_MANY_ARGUMENTS,
    NOT_ENOUGH_ARGUMENTS
};


void error_handler( const char *const message_errors[], size_t error_number );

enum main_arguments_status arguments_corresponding(size_t argc);

void main_arg_error_handler(enum main_arguments_status status);

#endif
