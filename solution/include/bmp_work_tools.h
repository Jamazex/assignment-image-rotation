#ifndef UNTITLED1_BMP_WORK_TOOLS_H
#define UNTITLED1_BMP_WORK_TOOLS_H

#include <stdbool.h>

#include "image_based.h"

enum read_status{
    READ_OK = 0,
    EMPTY_FILE,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_HEADER_ERROR,
    READ_BITS_ERROR
};

enum write_status{
    WRITE_OK = 0,
    WRITE_INVALID_SIGNATURE,
    WRITE_HEADER_ERROR,
    WRITE_BITS_ERROR,
    WRITE_PADDING_ERROR
};


bool open_bmp( const char* path, const char* mode, FILE** open );

enum read_status from_bmp( FILE *const in, struct image *const read );

enum write_status to_bmp( FILE *const out, const struct image *const img );

bool close_bmp(FILE *const close);

#endif
