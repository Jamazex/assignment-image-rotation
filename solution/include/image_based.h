#ifndef UNTITLED1_IMAGE_BASED_H
#define UNTITLED1_IMAGE_BASED_H

#include <stdint.h>
#include <malloc.h>

struct pixel{ uint8_t blue,green,red; };

struct image{
    uint64_t width, height;
    struct pixel* data;
};

struct image create_image( const uint64_t width, const uint64_t height );

void destroy_image( struct image image );

struct pixel get_elem(const struct image image, const size_t i, const size_t j);

#endif
