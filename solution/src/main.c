#include "image_rot_stub.h"
#include "utils.h"

int main(int argc, char** argv) {

    main_arg_error_handler(arguments_corresponding(argc));
    bmp_against_clockwise_rotate(argv[1], argv[2]);

    return 0;
}

