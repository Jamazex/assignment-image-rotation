#include <malloc.h>
#include <stdint.h>

struct __attribute__((packed)) pixel{ uint8_t blue,green,red; };

struct image{
    uint64_t width, height;
    struct pixel* data;
};

static const struct image empty_image = {0};

struct image create_image( const uint64_t width, const uint64_t height ){
    if (width > 0 && height > 0) return (struct image){
                .height = height,
                .width = width,
                .data = malloc(width * height * sizeof(struct pixel))
        };
    return empty_image;
}

void destroy_image( struct image image ){
    image.height = 0;
    image.width = 0;
    free(image.data);
}

struct pixel get_elem(const struct image image, const size_t i, const size_t j){
    return image.data[j*image.width + i];
}
