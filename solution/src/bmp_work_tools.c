#include <stdbool.h>

#include "image_based.h"
#include "utils.h"

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


enum read_status{
    READ_OK = 0,
    EMPTY_FILE,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_HEADER_ERROR,
    READ_BITS_ERROR
};

enum write_status{
    WRITE_OK = 0,
    WRITE_INVALID_SIGNATURE,
    WRITE_HEADER_ERROR,
    WRITE_BITS_ERROR,
    WRITE_PADDING_ERROR
};


static const uint16_t TYPE_BM = 0x4D42;
static const uint16_t PLANES_VALUE = 1;
static const uint16_t BIT_COUNT_VALUE = 24;
static const uint32_t INFOHEADER_SIZE = 40;
static const size_t BITS_IN_PIXEL = 3;
static const size_t BITS_STRING_MULTIPLICITY = 4;

static const struct bmp_header default_header = {
        .bfType = TYPE_BM,
        .bfileSize = 0,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = INFOHEADER_SIZE,
        .biWidth = 0,
        .biHeight = 0,
        .biPlanes = PLANES_VALUE,
        .biBitCount = BIT_COUNT_VALUE,
        .biCompression = 0,
        .biSizeImage = 0,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
};

static const uint8_t padding_bytes[3] = {0};

static bool header_is_invalid ( const struct bmp_header header ){
    if (header.bfType != default_header.bfType || header.bfReserved != default_header.bfReserved \
        || header.biSize != default_header.biSize || header.biPlanes != default_header.biPlanes \
        || header.biBitCount != default_header.biBitCount || header.bOffBits < default_header.bOffBits)
        return true;
    return false;
}

static struct bmp_header create_header( const struct image *const image, const size_t padding ){
    struct bmp_header header = default_header;
    header.biWidth = image->width;
    header.biHeight = image->height;
    header.biSizeImage = header.biWidth * header.biHeight * sizeof(struct pixel) + padding * header.biHeight;
    header.bfileSize = header.biSizeImage + header.bOffBits;
    return header;
}


static size_t get_padding(const struct image image ){
    size_t padding_size = 0;
    if (image.width % BITS_STRING_MULTIPLICITY) {
        padding_size = BITS_STRING_MULTIPLICITY - (BITS_IN_PIXEL * image.width) % BITS_STRING_MULTIPLICITY;
    }
    return padding_size;
}

bool open_bmp( const char* path, const char* mode, FILE** open ){
    return (*open = fopen(path, mode));
}

static bool header_read( struct bmp_header *const header, FILE *const from){
    return fread(header, sizeof(struct bmp_header), 1, from);
}

static bool pixels_read( FILE *const from, struct image *const image, const size_t i ){
    return fread(image->data + i*image->width, sizeof(struct pixel)*image->width, 1, from);
}

static bool header_write( const struct bmp_header *const header, FILE *const into){
    return fwrite(header, sizeof(struct bmp_header), 1, into);
}

static bool pixels_write( FILE *const into, const struct image *const image, const size_t i ){
    return fwrite(image->data + i*image->width, sizeof(struct pixel)*image->width, 1, into);
}

enum read_status from_bmp( FILE *const in, struct image *const read ){
    if (!in) return READ_INVALID_SIGNATURE;;

    //Header's reading and  error handling
    struct bmp_header header;
    if (!header_read(&header, in)) return READ_HEADER_ERROR;
    if (header_is_invalid(header)) return READ_INVALID_HEADER;
    if (!header.bfileSize) return EMPTY_FILE;

    //Image forming
    *read = create_image(header.biWidth, header.biHeight);

    //In case there is a color palette
    if (fseek(in, header.bOffBits - (uint32_t)(sizeof(header)), SEEK_CUR)) return READ_INVALID_HEADER;

    //Pixels amount valid check
    if ((size_t)(header.bfileSize - header.bOffBits) % sizeof(struct pixel)) return READ_INVALID_BITS;

    //Reading pixels with padding handling
    const size_t padding_size = get_padding(*read);
    for (size_t i = 0; i < read->height; i += 1) {
        if (!pixels_read(in, read, i) || fseek(in, (uint32_t) padding_size, SEEK_CUR)) {
            destroy_image(*read);
            return READ_BITS_ERROR;
        }
    }

    return  READ_OK;
}

enum write_status to_bmp( FILE *const out, const struct image *const img ){
    if (!out || !img) return WRITE_INVALID_SIGNATURE;

    //Header writing
    const size_t padding_size = get_padding(*img);
    struct bmp_header header = create_header(img, padding_size);
    if (!header_write(&header, out)) return WRITE_HEADER_ERROR;

    //Pixels writing
    for (size_t i = 0; i < img->height; i += 1){
        if (!pixels_write(out, img, i)) return WRITE_BITS_ERROR;
        if (!fwrite(&padding_bytes, padding_size,1, out)) return WRITE_PADDING_ERROR;
    }

    return WRITE_OK;
}

bool close_bmp(FILE *const close){
    return (fclose(close) == 0);
}
