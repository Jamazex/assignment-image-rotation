#include "bmp_work_tools.h"
#include "image_based.h"
#include "utils.h"

typedef struct pixel (direction)(struct image const source, const size_t i, const size_t j);

static const char *const opening_errors[] ={
        "ERROR:Opening file fault!\n"
};

static const char *const reading_errors[] ={
        "ERROR:Empty file\n",
        "ERROR:Invalid signature of reading function\n",
        "ERROR:Invalid bits\n",
        "ERROR:Invalid header\n",
        "ERROR:Reading header fault\n",
        "ERROR:Reading bits fault\n"
};

static const char *const writing_errors[] ={
        "ERROR:Invalid signature of writing function!\n",
        "ERROR:Writing header fault!\n",
        "ERROR:Writing bits fault!\n",
        "ERROR:Writing padding fault!\n"
};

static const char *const closing_errors[] ={
        "ERROR: Closing file fault!\n"
};

static struct pixel against_clockwise (const struct image source, const size_t i, const size_t j){
    return get_elem(source, j,source.height-i-1);
}

static struct image rotate( const struct image source, direction current_direction){
    struct image image = create_image(source.height, source.width);
    for (size_t j = 0; j < image.height; j += 1){
        for (size_t i = 0; i < image.width; i+= 1){
            image.data[j*image.width+ i] = current_direction(source, i, j);
        }
    }
    return image;
}

static void opening_error_handler(const bool open_status){
    error_handler(opening_errors, (size_t)(!open_status));
}
static void reading_error_handler(const enum read_status read_status, FILE* r_file){
    if (read_status) fclose(r_file);
    error_handler(reading_errors, (size_t)read_status);
}
static void writing_error_handler(const enum write_status write_status, FILE* w_file){
    if (write_status) fclose(w_file);
    error_handler(writing_errors, (size_t)write_status);
}
static void closing_error_handler(const bool close_status){
    error_handler(closing_errors, (size_t)(!close_status));
}


void bmp_against_clockwise_rotate(const char *const input_file, const char *const output_file){
    FILE* file_in;
    opening_error_handler(open_bmp(input_file, "rb", &file_in));
    struct image image_in;
    reading_error_handler(from_bmp(file_in, &image_in), file_in);
    closing_error_handler(close_bmp(file_in));
    FILE* file_out;
    struct image image_out = rotate(image_in,against_clockwise);
    destroy_image(image_in);
    opening_error_handler(open_bmp(output_file, "wb", &file_out));
    writing_error_handler(to_bmp(file_out, &image_out), file_out);
    destroy_image(image_out);
    closing_error_handler(close_bmp(file_out));
}
