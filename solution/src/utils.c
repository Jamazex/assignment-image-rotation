#include <stdio.h>
#include <stdlib.h>

enum main_arguments_status{
    ENOUGH_ARGUMENTS = 0,
    TOO_MANY_ARGUMENTS,
    NOT_ENOUGH_ARGUMENTS
};

const char *const main_arguments_errors[] ={
        "ERROR: Too many arguments got!\n",
        "ERROR: Not enough arguments got!\n"
};


static const size_t AMOUNT_OF_ARGUMENTS = 2;

void error_handler( const char *const message_errors[], const size_t error_number ){
    if (error_number){
        fprintf(stderr, "%s", message_errors[error_number-1]);
        exit((int)error_number);
    }
}

enum main_arguments_status arguments_corresponding(const size_t argc){
    if (argc > AMOUNT_OF_ARGUMENTS + 1) return TOO_MANY_ARGUMENTS;
    if (argc < AMOUNT_OF_ARGUMENTS + 1) return NOT_ENOUGH_ARGUMENTS;
    return ENOUGH_ARGUMENTS;
}

void main_arg_error_handler(const enum main_arguments_status status){
    error_handler(main_arguments_errors, (size_t)(status));
}

